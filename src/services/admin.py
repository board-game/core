from django.contrib import admin

from services.models import Room

admin.site.register(Room)
