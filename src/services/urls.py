from django.urls import path

from authentication.views import RegisterUser
from services.views import RoomApi, JoinRoom, GetRoomApi, PollApi, DeleteRoomApi, VoteApi, StatusApi, \
    IntroductionApi, KillApi, PlayersApi, CheckApi, KillMafiaApi, SaveApi, EndNightApi, DoctorApi, GodFatherApi, \
    GameStateApi, GetPollApi, EndDayApi

urlpatterns = [
    path('register/', RegisterUser.as_view(), name='register'),

    path('room/<str:room_slug>/', GetRoomApi.as_view(), name='get_room'),
    path('new-room/', RoomApi.as_view(), name='create_room'),
    path('delete-room/', DeleteRoomApi.as_view(), name='delete_room'),
    path('join/', JoinRoom.as_view(), name='join'),

    path('new-poll/', PollApi.as_view(), name='create_poll'),
    path('vote/', VoteApi.as_view(), name='new_vote'),
    path('poll/<str:room_slug>/', GetPollApi.as_view(), name='get_poll'),

    path('status/<str:chat_id>/', StatusApi.as_view(), name='get_role'),

    path('introduction-list/<str:room_slug>/', IntroductionApi.as_view(), name='introduction'),

    path('end-night/', EndNightApi.as_view(), name='end-night'),
    path('end-poll/', EndDayApi.as_view(), name='end-day'),
    path('save/', SaveApi.as_view(), name='save'),
    path('kill/', KillApi.as_view(), name='kill'),
    path('kill-mafia/', KillMafiaApi.as_view(), name='kill-mafia'),
    path('check/<str:room_slug>/<str:username>/', CheckApi.as_view(), name='check'),  # ignore the role
    path('game-state/<str:room_slug>/', GameStateApi.as_view(), name='game_state'),

    path('doctor/<str:room_slug>/', DoctorApi.as_view(), name='doctor'),
    path('god-father/<str:room_slug>/', GodFatherApi.as_view(), name='doctor'),

    path('players/', PlayersApi.as_view(), name='player_view')
]
