from collections import defaultdict

from django.test import TestCase
from django.urls import reverse
from authentication.models import TelUser, SubRecord, PlayerRoles
from services.models import Room
from services.views import RoleManager


class RoleManagerTest(TestCase):

    def _print_roles(self, num):
        queue = RoleManager._get_role_queue(num)
        d = defaultdict(int)
        for i in queue:
            d[i] += 1
        print(f'for {num} users roles are -> {d}')

    def test_print(self):
        for i in range(2, 13):
            self._print_roles(i)


    def test_game_state_api_more_mafia(self):
        room = Room.objects.create(num_of_players=3, admin_chat_id="admin_chat_id")

        players = []
        for _ in range(3):
            if _ < 2:
                players.append(TelUser.objects.create(chat_id=f"player{_}", role=PlayerRoles.MAFIA, username=f"player{_}"))
            else:
                players.append(TelUser.objects.create(chat_id=f"player{_}", role=PlayerRoles.CITIZEN, username=f"player{_}"))
        for player in players:
            SubRecord.objects.create(room=room, user=player)

        result = self.client.get(reverse('game_state', args=[room.name]))
        print(result)
