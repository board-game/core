from rest_framework import serializers

from authentication.models import Poll
from services.models import Room


class SubscribeSerializer(serializers.Serializer):
    chat_id = serializers.CharField(max_length=16)
    room_slug = serializers.CharField(max_length=8)


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ['num_of_players', 'admin_chat_id']


class DeleteRoomSerializer(serializers.Serializer):
    chat_id = serializers.CharField(max_length=16)
    room_slug = serializers.CharField(max_length=7)


class PollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
        fields = ['room_slug', 'name']


class VoteSerializer(serializers.Serializer):
    poll = serializers.CharField(max_length=7)
    vote_to = serializers.CharField(max_length=16, allow_null=True)
    voter = serializers.CharField(max_length=16)


class PlayerGetterSerializer(serializers.Serializer):
    selected_chat_id = serializers.CharField(max_length=16)
    room_slug = serializers.CharField(max_length=7)
    chat_id = serializers.CharField(max_length=16)


class RoomSendSerializer(serializers.Serializer):
    room_slug = serializers.CharField(max_length=7)


class PollSendSerializer(serializers.Serializer):
    poll_slug = serializers.CharField(max_length=7)
