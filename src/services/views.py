import math
import random
from collections import defaultdict

from rest_framework import generics
from rest_framework.exceptions import APIException
from django.db.models.query import F
from rest_framework import status
from rest_framework.response import Response
from django.utils.decorators import method_decorator
from authentication.models import SubRecord, TelUser, Poll, Vote, PlayerStatus, PlayerRoles
from authentication.serializers import PlayerSerializer
from services.models import Room, GameState, GameStage
from services.serializers import RoomSerializer, SubscribeSerializer, PollSerializer, DeleteRoomSerializer, \
    VoteSerializer, PlayerGetterSerializer, RoomSendSerializer, PollSendSerializer
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings


class GetRoomApi(generics.GenericAPIView):

    def get(self, request, *args, **kwargs):
        if slug := kwargs.get('room_slug', False):
            try:
                room = Room.objects.get(name=slug)
            except Room.DoesNotExist:
                return Response('room not found', status.HTTP_404_NOT_FOUND)
            filter_ = self.request.GET.get('filter', None)
            if filter_ is not None and filter_ not in PlayerRoles.get_filter().keys():
                return Response(data={
                    'status': f'bad filter. available choices {PlayerRoles.get_filter().keys()}'
                }, status=status.HTTP_400_BAD_REQUEST)
            users = []
            for record in SubRecord.objects.filter(room=room):
                if filter_ is not None:
                    if record.user.role in PlayerRoles.get_filter().get(filter_):
                        users.append(record.user.serialize())
                else:
                    users.append(record.user.serialize())
            return Response(data={
                'room': room.serialize(),
                'players': users
            })
        return Response('room not sent', status=status.HTTP_400_BAD_REQUEST)


class PlayersApi(generics.ListAPIView):
    serializer_class = PlayerSerializer

    def get_queryset(self):
        query_set = TelUser.objects.all()
        if room_slug := self.request.query_params.get('room', False):
            try:
                room = Room.objects.get(name=room_slug)
            except Room.DoesNotExist:
                raise APIException('room not found')
            records = SubRecord.objects.filter(room=room)
            chat_ids = [record.user.chat_id for record in records]
            query_set = query_set.filter(chat_id__in=chat_ids)
        if status_ := self.request.query_params.get('status', False):
            query_set = query_set.filter(status=status_.upper())
        if checked := self.request.query_params.get('check', False):
            query_set = query_set.filter(detective_checked=checked.lower() == 'true')
        return query_set

    def get(self, request, *args, **kwargs):
        resp = self.list(request, *args, **kwargs)
        return Response(data={'players': resp.data})


class CheckApi(generics.GenericAPIView):

    def get(self, request, *args, **kwargs):
        if room_slug := kwargs.get("room_slug"):
            try:
                room = Room.objects.get(name=room_slug)
            except Room.DoesNotExist:
                return Response(data={"status": "room does not exist"}, status=status.HTTP_404_NOT_FOUND)
            if first_name := kwargs.get('username', False):
                try:
                    record = SubRecord.objects.get(room=room, user__first_name=first_name)
                    is_bad = False
                    if record.user.role == PlayerRoles.MAFIA:
                        is_bad = True
                    return Response(data={'is_bad': is_bad}, status=status.HTTP_200_OK)
                except SubRecord.DoesNotExist:
                    return Response(data={"status": "user not in room"}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(data={"status": "user not exist"}, status=status.HTTP_404_NOT_FOUND)
        return Response(data={"status": "room not specified"}, status=status.HTTP_400_BAD_REQUEST)


class RoomApi(generics.GenericAPIView):
    serializer_class = RoomSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        try:
            user = TelUser.objects.get(chat_id=data['admin_chat_id'])
        except TelUser.DoesNotExist:
            return Response(data={
                'status': 'admin not registered yet'
            }, status=status.HTTP_400_BAD_REQUEST)

        instance = serializer.save()
        user.role = PlayerRoles.NOT_SPECIFIED
        user.status = PlayerStatus.ALIVE
        user.to_be_killed = False
        user.to_be_saved = False
        user.detective_checked = False
        user.save(update_fields=['role', 'status', 'detective_checked', 'to_be_saved', 'to_be_killed'])
        SubRecord.objects.create(room=instance, user=user)
        return Response(data={
            'name': instance.name
        }, status=status.HTTP_201_CREATED)


class DeleteRoomApi(generics.GenericAPIView):
    serializer_class = DeleteRoomSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        try:
            room = Room.objects.filter(name=data['room_slug'], admin_chat_id=data['chat_id'])
        except Room.DoesNotExist:
            return Response(data={'status': 'room not found'}, status=status.HTTP_400_BAD_REQUEST)

        for record in SubRecord.objects.filter(room=room):
            player = record.user
            player.role = PlayerRoles.NOT_SPECIFIED
            player.status = PlayerStatus.NOT_IN_GAME
            player.save(update_fields=['role', 'status'])

        room.delete()
        return Response(data={
            'name': room.name
        }, status=status.HTTP_200_OK)


@method_decorator(csrf_exempt, name='dispatch')
class JoinRoom(generics.GenericAPIView):
    serializer_class = SubscribeSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        try:
            user = TelUser.objects.get(chat_id=data['chat_id'])
            room = Room.objects.get(name=data['room_slug'])
        except Room.DoesNotExist:
            return Response(data={
                'data': 'room not found',
                'are_all_joined': False
            }, status=status.HTTP_404_NOT_FOUND)
        except TelUser.DoesNotExist:
            return Response(data={
                'data': 'user not registered yet',
                'are_all_joined': False
            }, status=status.HTTP_404_NOT_FOUND)

        if SubRecord.objects.filter(room=room, user=user).exists():
            return Response(data={
                'data': 'user already in room',
                'are_all_joined': False
            }, status=status.HTTP_400_BAD_REQUEST)

        curr_players_num = room.curr_players_num
        if curr_players_num == room.num_of_players:
            return Response(data='room already is full')

        user.role = PlayerRoles.NOT_SPECIFIED
        user.status = PlayerStatus.ALIVE
        user.to_be_killed = False
        user.to_be_saved = False
        user.detective_checked = False
        user.save(update_fields=['role', 'status', 'detective_checked', 'to_be_saved', 'to_be_killed'])
        SubRecord.objects.create(room=room, user=user)
        room.curr_players_num = F('curr_players_num') + 1
        room.save(update_fields=['curr_players_num'])

        users_in_game = []

        are_all_joined = room.num_of_players == curr_players_num + 1
        if are_all_joined:
            for record in SubRecord.objects.filter(room=room):
                users_in_game.append(record.user)
            RoleManager.assign_roles(users_in_game)

        return Response({
            'are_all_joined': are_all_joined
        }, status=status.HTTP_201_CREATED)


@method_decorator(csrf_exempt, name='dispatch')
class PollApi(generics.CreateAPIView):
    serializer_class = PollSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        slug = serializer.validated_data['room_slug']
        try:
            room = Room.objects.get(name=slug)
        except Room.DoesNotExist:
            return Response({'status': 'room has been deleted'}, status=status.HTTP_400_BAD_REQUEST)
        poll = Poll.objects.filter(room_slug=slug)
        if poll.exists():
            return Response({'status': 'a poll for this room is already created'}, status=status.HTTP_400_BAD_REQUEST)
        self.perform_create(serializer)
        alive_users = []
        records = SubRecord.objects.filter(room=room)
        for record in records:
            if record.user.status == PlayerStatus.ALIVE:
                alive_users.append(record.user)
        return Response(data={
            **serializer.data,
            'data': {
                player.chat_id: [p.serialize() for p in alive_users] for player in alive_users
            }
        })


class GetPollApi(generics.GenericAPIView):
    def get(self, request, *args, **kwargs):
        if room_slug := kwargs.get('room_slug', False):
            print(room_slug)
            try:
                Room.objects.get(name=room_slug)
                poll = Poll.objects.get(room_slug=room_slug)
            except Room.DoesNotExist:
                return Response(data={
                    'status': 'room not found'
                }, status=status.HTTP_404_NOT_FOUND)
            except Poll.DoesNotExist:
                return Response(data={
                    'status': 'no poll found'
                }, status=status.HTTP_404_NOT_FOUND)
            return Response(data=poll.serialize(), status=status.HTTP_200_OK)
        return Response(data={
            'status': 'room slug not sended'
        }, status=status.HTTP_404_NOT_FOUND)


@method_decorator(csrf_exempt, name='dispatch')
class VoteApi(generics.GenericAPIView):
    serializer_class = VoteSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        try:
            poll = Poll.objects.get(name=data['poll'])
            room = Room.objects.get(name=poll.room_slug)
            if voted_to := data.get('vote_to', False):
                if voted_to is not None:
                    voted_user = TelUser.objects.get(chat_id=voted_to)
            else:
                voted_user = None
            voter = TelUser.objects.get(chat_id=data['voter'])
        except Poll.DoesNotExist:
            return Response({'status': 'failed'}, status=status.HTTP_404_NOT_FOUND)
        except Room.DoesNotExist:
            return Response({'status': 'failed'}, status=status.HTTP_404_NOT_FOUND)
        except TelUser.DoesNotExist:
            return Response({
                'status': 'failed. user not exists'
            }, status=status.HTTP_404_NOT_FOUND)

        if Vote.objects.filter(voter=voter, poll=poll).exists():
            return Response({'status': 'already voted'}, status=status.HTTP_400_BAD_REQUEST)

        if voted_to:
            if not SubRecord.objects.filter(room=room, user=voted_user).exists():
                return Response(data={
                    'status': 'failed. voted user is not in game'
                }, status=status.HTTP_400_BAD_REQUEST)
        Vote.objects.create(poll=poll, voter=voter, vote_to=voted_user)
        records = SubRecord.objects.filter(room=room)
        alive_players_num = 0
        for record in records:
            if record.user.status != PlayerStatus.DEATH:
                alive_players_num += 1
        return Response(data={
            'status': 'voted',
            'voted_num': poll.votes.count(),
            'players_num': room.num_of_players,
            'alive_players_num': alive_players_num,
            'is_poll_ended': poll.votes.count() == alive_players_num
        }, status=status.HTTP_200_OK)


class StatusApi(generics.GenericAPIView):
    def get(self, request, *args, **kwargs):
        if chat_id := kwargs.get('chat_id', False):
            try:
                user = TelUser.objects.get(chat_id=chat_id)
            except TelUser.DoesNotExist:
                return Response(data={
                    'status': 'user not registered'
                }, status=status.HTTP_400_BAD_REQUEST)
            return Response(data={
                **user.serialize(),
                'is_alive': user.status == PlayerStatus.ALIVE
            }, status=status.HTTP_200_OK)
        return Response(data={
            'status': 'not found'
        }, status=status.HTTP_404_NOT_FOUND)


class IntroductionApi(generics.GenericAPIView):
    def get(self, request, *args, **kwargs):
        if slug := kwargs.get('room_slug', False):
            try:
                room = Room.objects.get(name=slug)
            except Room.DoesNotExist:
                return Response(data={
                    'status': 'room not found'
                }, status=status.HTTP_404_NOT_FOUND)
            bad_players = list()
            resp = dict()
            for record in SubRecord.objects.filter(room=room):
                player = record.user
                resp[player.chat_id] = []
                if player.role in PlayerRoles.get_filter().get(PlayerRoles.BAD):
                    bad_players.append(player)
            for user in bad_players:
                resp[user.chat_id] = [use.serialize() for use in bad_players if use.chat_id != user.chat_id]
            return Response(data={'data': resp}, status=status.HTTP_200_OK)
        else:
            return Response(data={
                'status': 'room slug not sended'
            }, status=status.HTTP_400_BAD_REQUEST)


class KillApi(generics.GenericAPIView):
    serializer_class = PlayerGetterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        try:
            room = Room.objects.get(name=data['room_slug'])
            user = TelUser.objects.get(first_name=data['username'])
            SubRecord.objects.get(room=room, user=user)
        except Room.DoesNotExist:
            return Response({'status': 'room not existed'}, status=status.HTTP_404_NOT_FOUND)
        except TelUser.DoesNotExist:
            return Response({'status': 'user not existed'}, status=status.HTTP_404_NOT_FOUND)
        except SubRecord.DoesNotExist:
            return Response({
                'status': 'this user not in this game or game not found'
            }, status=status.HTTP_404_NOT_FOUND)
        user.status = PlayerStatus.DEATH
        user.save(update_fields=['status'])
        return Response(data={'status': 'ok'}, status=status.HTTP_200_OK)


class KillMafiaApi(generics.GenericAPIView):
    serializer_class = PlayerGetterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        try:
            room = Room.objects.get(name=data['room_slug'])
            if room.game_stage == GameStage.DAY:
                return Response(
                    data={'status': 'Can only occur at night time'}, status=status.HTTP_400_BAD_REQUEST
                )
            user = TelUser.objects.get(chat_id=data['selected_chat_id'])
            father = TelUser.objects.get(chat_id=data['chat_id'])
            SubRecord.objects.get(room=room, user=user)
            SubRecord.objects.get(room=room, user=father)
        except Room.DoesNotExist:
            return Response({'status': 'room not existed'}, status=status.HTTP_404_NOT_FOUND)
        except TelUser.DoesNotExist:
            return Response({'status': 'user not existed'}, status=status.HTTP_404_NOT_FOUND)
        except SubRecord.DoesNotExist:
            return Response({
                'status': 'this user not in this game or game not found'
            }, status=status.HTTP_404_NOT_FOUND)
        if father.role != PlayerRoles.GOD_FATHER:
            return Response({'status': 'only god father can choose'}, status=status.HTTP_400_BAD_REQUEST)
        user.to_be_killed = True
        user.save(update_fields=['to_be_killed'])
        return Response(data={'status': 'ok'}, status=status.HTTP_200_OK)


class SaveApi(generics.GenericAPIView):
    serializer_class = PlayerGetterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        try:
            room = Room.objects.get(name=data['room_slug'])
            if room.game_stage != GameStage.NIGHT:
                return Response(
                    data={'status': 'Saving can only occur at night time'},
                    status=status.HTTP_400_BAD_REQUEST
                )
            savee = TelUser.objects.get(chat_id=data['selected_chat_id'])
            savior = TelUser.objects.get(chat_id=data['chat_id'])
            SubRecord.objects.get(room=room, user=savee), SubRecord.objects.get(room=room, user=savior)
        except Room.DoesNotExist:
            return Response({'status': 'room not existed'}, status=status.HTTP_404_NOT_FOUND)
        except TelUser.DoesNotExist:
            return Response({'status': 'user not existed'}, status=status.HTTP_404_NOT_FOUND)
        except SubRecord.DoesNotExist:
            return Response({
                'status': 'this user not in this game or game not found'
            }, status=status.HTTP_404_NOT_FOUND)

        if savior.role == PlayerRoles.DOCTOR:
            if SubRecord.objects.filter(room=room, user__to_be_saved=True).exists():
                return Response(data={
                    'status': 'Chat ID has saved someone already'
                }, status=status.HTTP_400_BAD_REQUEST)
            savee.to_be_saved = True
            savee.save(update_fields=['to_be_saved'])
            return Response(data={'status': 'ok'}, status=status.HTTP_200_OK)
        else:
            return Response(data={'status': 'chat id not allowed to save player'}, status=status.HTTP_400_BAD_REQUEST)


class EndDayApi(generics.GenericAPIView):
    serializer_class = PollSendSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        try:
            poll = Poll.objects.get(name=data['poll_slug'])
            room = Room.objects.get(name=poll.room_slug)
        except Poll.DoesNotExist:
            return Response(data={'status': 'room not existed'}, status=status.HTTP_404_NOT_FOUND)

        if room.game_stage != GameStage.DAY:
            return Response(
                data={"status": "Can't change night to night"}, status=status.HTTP_400_BAD_REQUEST
            )

        try:
            poll = Poll.objects.get(room_slug=room.name)
        except Poll.DoesNotExist:
            return Response(data={
                'status': 'not poll has been created yet'
            }, status=status.HTTP_400_BAD_REQUEST)

        if poll.votes.count() != room.num_of_players:
            return Response(data={'status': 'poll not over yet'}, status=status.HTTP_400_BAD_REQUEST)

        votes = poll.votes.all()
        d = defaultdict(int)
        for vote in votes:
            if vote.vote_to is not None:
                d[vote.vote_to.chat_id] += 1
        nums = list(d.values())
        nums.sort()

        print(d)

        alive_players = [record.user for record in SubRecord.objects.filter(room=room, user__status=PlayerStatus.ALIVE)]

        if len(votes) != len(alive_players):
            return Response(data={'status': 'everyone must vote first'}, status=status.HTTP_400_BAD_REQUEST)
        if len(nums) == 0 or nums[-1] < len(alive_players) / 2:
            votes.delete()
            return Response(data={
                'status': 'no one died',
                'killed_players': [],
                'results': [{**player.serialize(), 'votes': 0} for player in alive_players]
            }, status=status.HTTP_200_OK)

        ci = None
        for chat_id, num in d.items():
            if num == nums[-1]:
                ci = chat_id
                break

        user = TelUser.objects.get(chat_id=ci)
        user.status = PlayerStatus.DEATH
        user.save(update_fields=['status'])

        poll.delete()
        room.game_stage = GameStage.NIGHT
        room.save(update_fields=['game_stage'])

        results = []
        for player in alive_players:
            results.append({
                **player.serialize(),
                'votes': d.get(player.chat_id, 0)
            })

        print(results)

        return Response(data={
            'status': 'someone died',
            'results': results,
            'killed_players': [user.serialize()]
        }, status=status.HTTP_200_OK)


class EndNightApi(generics.GenericAPIView):
    serializer_class = RoomSendSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data

        try:
            room = Room.objects.get(name=data['room_slug'])
        except Room.DoesNotExists:
            return Response(data={
                'status': 'room not existed',
                'night_ended': False
            }, status=status.HTTP_404_NOT_FOUND)

        if room.game_stage != GameStage.NIGHT:
            return Response(data={
                "status": "Can't change day to day",
                'night_ended': False
            }, status=status.HTTP_400_BAD_REQUEST)

        if SubRecord.objects.filter(room=room, user__role=PlayerRoles.DOCTOR).exists():
            if not SubRecord.objects.filter(room=room, user__to_be_saved=True).exists():
                return Response(data={
                    'status': 'doctor has not chosen',
                    'night_ended': False
                }, status=status.HTTP_400_BAD_REQUEST)
        if not SubRecord.objects.filter(room=room, user__to_be_killed=True).exists():
            return Response(data={
                'status': 'god father has not chosen',
                'night_ended': False
            }, status=status.HTTP_400_BAD_REQUEST)

        dead_players = []
        for record in SubRecord.objects.filter(room=room):
            player = record.user
            if player.status == PlayerStatus.ALIVE:
                if player.to_be_killed and not player.to_be_saved:
                    dead_players.append(player.serialize())
                    player.status = PlayerStatus.DEATH
                    player.save(update_fields=['status'])

            player.to_be_killed = False
            player.to_be_saved = False
            player.save(update_fields=['to_be_killed', 'to_be_saved'])

        room.game_stage = GameStage.DAY
        room.save(update_fields=['game_stage'])

        return Response(data={
            'dead_players': dead_players,
            'status': f'{room.name} changed from night to day.',
            'night_ended': True
        }, status=status.HTTP_200_OK)


class GameStateApi(generics.GenericAPIView):
    def get(self, request, *args, **kwargs):
        if slug := kwargs.get('room_slug', False):
            try:
                room = Room.objects.get(name=slug)
            except Room.DoesNotExist:
                return Response(data={
                    'status': 'room not found'
                }, status=status.HTTP_404_NOT_FOUND)

            godfather_is_alive = False
            recs = SubRecord.objects.filter(room=room, user__status=PlayerStatus.ALIVE)
            players = {"good": [], "bad": []}
            for rec in recs:
                player = rec.user
                if player.role == PlayerRoles.GOD_FATHER:
                    godfather_is_alive = True

                if player.role in PlayerRoles.get_filter().get(PlayerRoles.GOOD):
                    players['good'].append(player)
                else:
                    players['bad'].append(player)

            if len(players['good']) <= len(players['bad']):
                room.state = GameState.MAFIA_VICTORY
            elif len(players['good']) == len(players['bad']) + 1:
                room.state = GameState.CRITICAL
            elif len(players['bad']) <= 0:
                room.state = GameState.MAFIA_LOSS

            if not godfather_is_alive:
                room.state = GameState.MAFIA_LOSS

            room.save(update_fields=['state'])
            return Response(data={'state': room.state}, status=status.HTTP_200_OK)

        else:
            return Response(data={
                'status': 'room slug not sended'
            }, status=status.HTTP_400_BAD_REQUEST)


class DoctorApi(generics.GenericAPIView):

    def get(self, request, *args, **kwargs):
        if room_slug := kwargs.get("room_slug"):
            try:
                room = Room.objects.get(name=room_slug)
                players = []
                doctor = None
                doctor_is_alive = False
                for record in SubRecord.objects.filter(room=room):
                    players.append(record.user)
                    if record.user.role == PlayerRoles.DOCTOR:
                        doctor = record.user
                        if doctor.status == PlayerStatus.ALIVE:
                            doctor_is_alive = True
                available_players = [player.serialize() for player in players if player.status == PlayerStatus.ALIVE]
                return Response(data={
                    'is_alive': doctor_is_alive,
                    'doctor': doctor.serialize(),
                    'available_players': available_players
                }, status=status.HTTP_200_OK)
            except Room.DoesNotExist:
                return Response(data={'status': 'room not existed'}, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(data={"status": "room does not exist"}, status=status.HTTP_404_NOT_FOUND)


class GodFatherApi(generics.GenericAPIView):

    def get(self, request, *args, **kwargs):
        if room_slug := kwargs.get("room_slug"):
            try:
                room = Room.objects.get(name=room_slug)
                players = []
                god_father = None
                for record in SubRecord.objects.filter(room=room):
                    players.append(record.user)
                    if record.user.role == PlayerRoles.GOD_FATHER:
                        god_father = record.user
                alive_players = [player for player in players
                                 if player.status == PlayerStatus.ALIVE and player.role != PlayerRoles.MAFIA]
                alive_players.remove(god_father)
                alive_players = list(map(TelUser.serialize, alive_players))
                return Response(data={
                    'is_alive': god_father.status == PlayerStatus.ALIVE,
                    'god_father': god_father.serialize(),
                    'available_players': alive_players
                }, status=status.HTTP_200_OK)
            except Room.DoesNotExists:
                return Response(data={'status': 'room not existed'}, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(data={"status": "room does not exist"}, status=status.HTTP_404_NOT_FOUND)


class RoleManager:

    @staticmethod
    def assign_roles(user_list):
        size = len(user_list)
        queue = RoleManager._get_role_queue(size)
        RoleManager.set_roles(user_list, queue)

    @staticmethod
    def _get_role_queue(size):
        queue = list()
        queue.append(PlayerRoles.GOD_FATHER)
        queue.append(PlayerRoles.DOCTOR)
        if size > 5:
            pass
            # queue.append(PlayerRoles.DETECTIVE)
        for _ in range(int(math.floor(size * settings.MAFIA_PERCENT)) - 2):
            queue.append(PlayerRoles.MAFIA)
        while len(queue) < size:
            queue.append(PlayerRoles.CITIZEN)
        return queue

    @staticmethod
    def set_roles(user_list, queue):
        while len(user_list) > 0:
            role = queue.pop(0)
            user = random.choice(user_list)
            user.role = role
            user.save(update_fields=['role'])
            user_list.remove(user)
