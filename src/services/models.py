import random
import string

from django.db import models


def generate_random():
    slug = ''.join(random.choice(string.digits) for _ in range(3))
    if Room.objects.filter(name=slug).exists():
        return generate_random()
    return slug


class GameState:
    MAFIA_VICTORY = "mafia"
    MAFIA_LOSS = "citizen"
    NORMAL = "normal"
    CRITICAL = "crit"

    @staticmethod
    def get_choices():
        return (
            (GameState.MAFIA_VICTORY, GameState.MAFIA_VICTORY),
            (GameState.MAFIA_LOSS, GameState.MAFIA_LOSS),
            (GameState.NORMAL, GameState.NORMAL),
            (GameState.CRITICAL, GameState.CRITICAL)
        )


class GameStage:
    DAY = "D"
    NIGHT = "N"

    @staticmethod
    def get_choices():
        return (
            (GameStage.DAY, GameStage.DAY),
            (GameStage.NIGHT, GameStage.NIGHT),
        )


class Room(models.Model):
    num_of_players = models.IntegerField()
    admin_chat_id = models.CharField(max_length=16)
    name = models.CharField(max_length=7, default=generate_random)
    curr_players_num = models.IntegerField(default=1)
    state = models.CharField(max_length=8, choices=GameState.get_choices(), default=GameState.NORMAL)
    game_stage = models.CharField(max_length=10, choices=GameStage.get_choices(), default=GameStage.NIGHT)

    def serialize(self):
        return {
            'num_of_players': self.num_of_players,
            'admin_chat_id': self.admin_chat_id,
            'slug': self.name,
            'current_player_num': self.curr_players_num
        }

    def __str__(self):
        return f'{self.name}, {self.curr_players_num} of {self.num_of_players} are in'
