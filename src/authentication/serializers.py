import random
import string

from rest_framework import serializers

from authentication.models import TelUser


def generate_random_username():
    return ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits)
                   for _ in range(20))


class RegisterUserSerializer(serializers.ModelSerializer):

    def validate(self, attrs):
        attrs['username'] = generate_random_username()
        return attrs

    class Meta:
        model = TelUser
        fields = ['chat_id', 'first_name']


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = TelUser
        fields = [
            'first_name',
            'chat_id',
            'status',
            'role',
            'detective_checked'
        ]
