from django.contrib import admin

from authentication.models import TelUser, SubRecord, Poll, Vote

admin.site.register(TelUser)
admin.site.register(SubRecord)
admin.site.register(Poll)
admin.site.register(Vote)
