# Generated by Django 3.2.9 on 2021-11-25 19:08

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0006_auto_20211125_1401'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vote',
            name='vote_to',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='voted_user', to=settings.AUTH_USER_MODEL),
        ),
    ]
