import random
import string

from django.contrib.auth.models import AbstractUser
from django.db import models

from services.models import Room


def generate_random():
    slug = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(6))
    if Poll.objects.filter(name=slug).exists():
        return generate_random()
    return slug


class PlayerStatus:
    NOT_IN_GAME = 'NOT_IN_GAME'
    ALIVE = 'ALIVE'
    DEATH = 'DEATH'
    SILENT = 'SILENT'

    @staticmethod
    def get_choices():
        return (
            (PlayerStatus.ALIVE, PlayerStatus.ALIVE),
            (PlayerStatus.DEATH, PlayerStatus.DEATH),
            (PlayerStatus.SILENT, PlayerStatus.SILENT),
            (PlayerStatus.NOT_IN_GAME, PlayerStatus.NOT_IN_GAME)
        )


class PlayerRoles:
    GOD_FATHER = 'GOD_FATHER'
    MAFIA = 'MAFIA'
    DOCTOR = 'DOCTOR'
    CITIZEN = 'CITIZEN'
    DETECTIVE = 'DETECTIVE'
    NOT_SPECIFIED = 'NOT_SPECIFIED'

    BAD = 'bad'
    GOOD = 'good'

    @staticmethod
    def get_filter():
        return {
            PlayerRoles.BAD: [PlayerRoles.MAFIA, PlayerRoles.GOD_FATHER],
            PlayerRoles.GOOD: [PlayerRoles.DOCTOR, PlayerRoles.CITIZEN, PlayerRoles.DETECTIVE]
        }

    @staticmethod
    def get_choices():
        return (
            (PlayerRoles.GOD_FATHER, PlayerRoles.GOD_FATHER),
            (PlayerRoles.MAFIA, PlayerRoles.MAFIA),
            (PlayerRoles.CITIZEN, PlayerRoles.CITIZEN),
            (PlayerRoles.DOCTOR, PlayerRoles.DOCTOR),
            (PlayerRoles.DETECTIVE, PlayerRoles.DETECTIVE),
            (PlayerRoles.NOT_SPECIFIED, PlayerRoles.NOT_SPECIFIED)
        )


class TelUser(AbstractUser):
    chat_id = models.CharField(max_length=16, unique=True)
    status = models.CharField(
        max_length=11,
        choices=PlayerStatus.get_choices(),
        default=PlayerStatus.NOT_IN_GAME
    )
    role = models.CharField(
        max_length=16,
        choices=PlayerRoles.get_choices(),
        default=PlayerRoles.NOT_SPECIFIED
    )
    to_be_killed = models.BooleanField(default=False)
    to_be_saved = models.BooleanField(default=False)
    detective_checked = models.BooleanField(default=False)

    USERNAME_FIELD = 'chat_id'
    REQUIRED_FIELDS = ['username', 'password', 'first_name']

    def serialize(self):
        return {
            'name': self.first_name,
            'chat_id': self.chat_id,
            'status': self.status,
            'role': self.role,
            'to_be_killed': self.to_be_killed,
            'to_be_saved': self.to_be_saved
        }

    def __str__(self):
        return self.first_name


class SubRecord(models.Model):
    room = models.ForeignKey(Room, models.CASCADE, related_name='record_room')
    user = models.ForeignKey(TelUser, models.CASCADE, related_name='record_user')


class Poll(models.Model):
    room_slug = models.CharField(max_length=7)
    name = models.CharField(max_length=7, default=generate_random)

    def serialize(self):
        return {
            'room_slug': self.room_slug,
            'name': self.name,
            'votes': [vote.serialize() for vote in self.votes.all()]
        }

    def __str__(self):
        return self.name


class Vote(models.Model):
    poll = models.ForeignKey(Poll, models.CASCADE, related_name='votes')
    voter = models.ForeignKey(TelUser, models.CASCADE, related_name='voter_user')
    vote_to = models.ForeignKey(TelUser, models.CASCADE, related_name='voted_user', null=True)

    def serialize(self):
        return {
            'poll': self.poll.name,
            'voter': self.voter.serialize(),
            'vote_to': self.vote_to.serialize() if self.vote_to is not None else None
        }

    def __str__(self):
        return self.voter.first_name
