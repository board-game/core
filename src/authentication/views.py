from rest_framework import generics
from authentication.serializers import RegisterUserSerializer


class RegisterUser(generics.CreateAPIView):
    serializer_class = RegisterUserSerializer
