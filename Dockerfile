FROM m.docker-registry.ir/python:3.9

RUN apt update
RUN apt install --fix-missing -y netcat

ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY ./entrypoint.sh /
COPY ./src /code/

ENTRYPOINT ['/entrypoint.sh']
